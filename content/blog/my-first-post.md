---
title: "Under Construction"
date: 2019-09-19T14:10:04-04:00
tags: [unnecessary, new]
description: "This is a page for helping to adjust style"
draft: false
---
## Oh, Hi

Didn't see you there!

Don't mind me, just building a site for my CV!

## Listless Lists
1. An item
2. Another one
3. Another one
4. Another one
5. Another one
6. Another one

* Some other items
* In no particular order
* Well it's kinda implied
* But whatever

## A Little Code for Practice

{{< highlight bash >}}

for i in $(ls); do
	echo Hello $i
done

{{< /highlight >}}

## And a Block Quote While We're At It...
>Hewwo?
>This is a bwock qwote
>Let's hope it shows up right!

## Tables are Tedious
Header 1 | Header 2
---------|----------
I Love   | You!
OWO	 | UWU
