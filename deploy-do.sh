#!/bin/sh
USER=root
HOST=box.elijahlove.xyz
DIR=/home/user-data/www/default

hugo && rsync -avz --delete public/ ${USER}@${HOST}:${DIR}

exit 0
