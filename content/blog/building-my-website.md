---
title: "Building My Website"
date: 2019-10-07T18:44:08-04:00
tags: [website, hugo, git, hosting]
description: "My Decision-Making Process for Constructing a Website for my Resume and Blog"
draft: false
---

I need my personal website to show the world few simple things:

- My Resume
- My Blog
- My Contact Info

There aren't many qualities that I ask for from a website meant to represent myself:

- Clean
- Fast
- Portable
- Low-Maintenance

Content management systems like WordPress and Joomla feel clunky and overfeatured when the only things I need are a place to throw my resume and maybe a few blog posts.
I also view them as giant security headaches that I really don't want to spend time dealing with.

Going with handwritten HTML isn't an option for me, either; it's difficult to manage beyond a couple of pages, and it's very vanilla.

A static site generator felt like the right choice; all the complicated content management stuff is done one time before uploaded to an HTTP server.
No databases, no server-side scripting, no problem.
If I ever want do something fancy like [publish to IPFS](https://withblue.ink/2019/03/20/hugo-and-ipfs-how-this-blog-works-and-scales.html), I don't have to worry about dealing with icky application logic and stuff.
